import os
import io
import functools
import urllib.parse
import urllib.request

import joblib
import pandas as pd
import numpy as np

import biomart

GTEx_links = {
    "sample_annotation": 
            "annotations/GTEx_Analysis_v8_Annotations_SampleAttributesDS.txt",
    "subject_annotation": 
        "annotations/GTEx_Analysis_v8_Annotations_SubjectPhenotypesDS.txt",
    "expression": 
        "rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_reads.gct.gz"
}

GTEx_prefix = "https://storage.googleapis.com/gtex_analysis_v8/"
GTEx_links = {k:urllib.parse.urljoin(GTEx_prefix, v) for k,v in GTEx_links.items()}

memoize = joblib.Memory(location="cache/").cache

def download(URL):
    fname = os.path.basename(urllib.parse.urlparse(URL).path)
    if not os.path.exists(fname):
        try:
            urllib.request.urlretrieve(URL, fname)
        except Exception as e:
            print(e)
            if os.path.exists(fname):
                os.unlink(fname)
    return fname

@functools.lru_cache()
def gene_metadata():
    server = biomart.BiomartServer("http://uswest.ensembl.org/biomart")
    dataset = server.datasets["hsapiens_gene_ensembl"]
    attrs = ["ensembl_gene_id", "chromosome_name", 
            "hgnc_symbol", "entrezgene_description"]
    response = dataset.search({"attributes": attrs})
    with io.StringIO(response.text) as h:
        o = pd.read_table(h, header=None)
        o.columns = ["GeneID", "Chromosome", "Symbol", "Name"]
        return o.drop_duplicates(subset=["GeneID"]).set_index(["GeneID"])

def matrix_transformer(fn):
    """
    A decorator for a function that takes a 2D ndarray or DataFrame as
    its only positional argument (kwargs are allowed) and returns a 
    2D ndarray of the same size.

    If a :class:`pandas.DataFrame` was supplied, this decorator will
    wrap the output 2D array by adding the appropriate labels.
    """
    @functools.wraps(fn)
    def wrap(Xi, **kwargs):
        assert len(Xi.shape) == 2
        if isinstance(Xi, pd.DataFrame):
            Xim = np.array(Xi)
            Xo = fn(Xim, **kwargs)
            assert (Xi.shape == Xo.shape)
            o = pd.DataFrame(Xo, index=Xi.index, columns=Xi.columns)
            o.index.name = Xi.index.name
            o.columns.name = Xi.columns.name
            return o
        elif isinstance(Xi, np.ndarray):
            return fn(Xi, **kwargs)
        else:
            raise ValueError
    return wrap

@matrix_transformer
def quantile(X, mu=None):
    """
    Quantile normalize a matrix.

    Parameters
    ----------
    X : a 2D :class:`numpy.ndarray`
        The matrix to be normalized, with samples as columns
        and probes/genes as rows.
    mu : a 1D :class:`numpy.ndarray`, optional
        Vector of gene means.

    Returns
    -------
    :class:`numpy.ndarray`
        The normalized matrix.
    """
    # transposed, so samples are rows
    assert not np.isnan(X).all(axis=1).any() # rows
    if mu is not None:
        mu = np.array(mu)
        assert len(mu) == X.shape[1] 
        assert not np.isnan(mu).any()
    else:
        assert not np.isnan(X).all(axis=0).any() # columns

    Xm = np.ma.masked_invalid(X.T)
    Xn = np.empty(Xm.shape)
    Xn[:] = np.nan

    if mu is None:
        mu = Xm.mean(axis=0)
    mu.sort()

    for i in range(Xm.shape[0]):
        # sort and argsort sorts small to large with NaN at the end
        ix = np.argsort(Xm[i,:])
        nok = (~Xm[i,:].mask).sum()
        ix = ix[:nok]
        rix = (np.arange(nok) * len(mu) / nok).round().astype(int)
        Xn[i,ix] = mu[rix]

    Xn = Xn.T
    assert (np.isnan(X) == np.isnan(Xn)).all()
    return Xn

def sample_annotation():
    path = download(GTEx_links["sample_annotation"])
    return pd.read_csv(path, sep="\t", index_col=0)

def subject_annotation():
    path = download(GTEx_links["subject_annotation"])
    return pd.read_csv(path, sep="\t", index_col=0)

@functools.lru_cache()
def metadata():
    get_subject_id = lambda x: "-".join(x.split("-")[:2])
    A = sample_annotation()
    A["SubjectID"] = [get_subject_id(x) for x in A.index]
    A_subject = subject_annotation()
    o = A.merge(A_subject, left_on="SubjectID", right_index=True)
    o = o.loc[:,["SubjectID", "SEX","AGE","SMTS","DTHHRDY"]]
    o.columns = ["SubjectID", "Sex","Age","Tissue","DeathScale"]
    M = {2:0,1:1}
    o.Sex = [M.get(x, np.nan) for x in o.Sex]
    def get_age(x):
        start, end = x.split("-")
        return np.mean([int(start),int(end)])
    o.Age = list(map(get_age, o.Age))
    return o
 
@functools.lru_cache()
def expression(nrows=10000, **kwargs):
    path = download(GTEx_links["expression"])
    kwargs = {"nrows": nrows} if nrows is not None else {}
    X = pd.read_csv(path, index_col=0, skiprows=2, 
            sep="\t", engine="c", compression="gzip", **kwargs)\
            .iloc[:,1:]
    X.index = [ix.split(".")[0] for ix in X.index]
    X = X.T
    X = X.loc[:,X.mean() > 1]
    return X

def data():
    A = metadata()
    A = A.loc[A["Tissue"].isin(A["Tissue"].value_counts().index[:10]),:]
    X = expression()
    G = gene_metadata()
    A,X = A.align(X, axis=0, join="inner")
    G,X = G.align(X.T, axis=0, join="inner")
    X = X.T
    return A,G,X
