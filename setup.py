from setuptools import setup, find_packages

setup(
    name="DSS2021_L1",
    version="1.0.0",
    description="Auxiliary code for OUHSC DSS 2021 lab",
    author="Cory Giles",
    author_email="gilesc@omrf.org",
    py_modules=["DSS2021_L1"],
    install_requires=["umap-learn", "biomart", "upsetplot", "biopython"]
)
